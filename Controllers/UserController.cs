﻿using CryptSharp;
using GameOfLife.Models.Database;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace GameOfLife.Controllers
{
    public class UserController : Controller
    {
        private GameOfLifeEntities db = new GameOfLifeEntities();

        // GET: User/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: User/Register
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User user)
        {
            if (ModelState.IsValid)
            {
                user.IsAdmin = false;
                byte[] passwordStringToBytes = Encoding.ASCII.GetBytes(user.Password);
                user.Password = Crypter.Blowfish.Crypt(passwordStringToBytes);
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(user);
        }

        // GET: User/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: User/Login
        [HttpPost]
        [ActionName("Login")]
        public ActionResult LoginPost(User user)
        {
            using (var db = new GameOfLifeEntities())
            {
                User login = db.Users.FirstOrDefault(u => u.Email == user.Email);  
                                                               
                if (login != null && Crypter.CheckPassword(user.Password, login.Password)) 
                {
                    Session["User"] = login;

                    return RedirectToAction("Index", "Home");
                }
            }

            return View(user);
        }

        public ActionResult Logout()
        {
            // Kill the session variables & return to login page
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
