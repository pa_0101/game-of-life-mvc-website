﻿using GameOfLife.Models;
using GameOfLife.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GameOfLife.Controllers
{
    public class UserGameController : Controller
    {
        private GameOfLifeEntities db = new GameOfLifeEntities();
        private static List<UserGame> listOfUserGames = new List<UserGame>();

        // GET: UserGame/CreateActiveGameStep1
        public ActionResult CreateActiveGameStep1()
        {
            var templates = new List<SelectListItem>();
            foreach (var x in db.UserTemplates.ToList())
            {
                templates.Add(new SelectListItem
                {
                    Text = x.Name,
                    Value = x.UserTemplateID.ToString()
                });
            }
            ViewBag.Templates = templates;

            return View();
        }

        // GET: UserGame/CreateActiveGameStep2
        public ActionResult CreateActiveGameStep2(int? templateID)
        {
            if (templateID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(templateID);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }

            return View(userTemplate);
        }

        // POST: UserGame/CreateActiveGameStep2
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateActiveGameStep2(UserGame userGame/*, int? templateID*/)
        {
            if (ModelState.IsValid)
            {
                listOfUserGames.Add(userGame);
                Session["ListOfUserGames"] = listOfUserGames;
                Session["UserGame"] = userGame;

                return RedirectToAction("MyActiveGames", "UserGame");
            }

            return View();
        }

        // GET: UserGame/MyActiveGames
        public ActionResult MyActiveGames()
        {
            return View();
        }

        // GET: UserGame/DeleteGame/5
        public ActionResult DeleteGame(string name)
        {
            UserGame userGame = null;
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            foreach (var sessionItem in listOfUserGames)
            {
                if (sessionItem.Name.Equals(name))
                {
                    userGame = sessionItem;
                    break;
                }
            }
            if (userGame == null)
            {
                return HttpNotFound();
            }

            return View(userGame);
        }

        // POST: UserGame/Delete/5
        [HttpPost, ActionName("DeleteGame")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string name)
        {
            foreach (var sessionItem in listOfUserGames)
            {
                if (sessionItem.Name.Equals(name))
                {
                    listOfUserGames.Remove(sessionItem);
                    break;
                }
            }

            return RedirectToAction("MyActiveGames", "UserGame");
        }

        // GET: UserGame/DeleteGame/5
        public ActionResult DeleteSavedGame(int id)
        {
            UserGame savedGameToDelete = db.UserGames.Find(id);
            db.UserGames.Remove(savedGameToDelete);
            db.SaveChanges();

            return RedirectToAction("MySavedGames", "UserGame");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public Cell[][] ConvertTo2dCellArray(int height, int width, string gameCells)
        {
            int colIndex = 0;
            int rowIndex = 0;
            Cell[][] cells = new Cell[height][];
            Cell[] row = new Cell[width];

            string[] cellRows = gameCells.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (cellRows != null)
            {
                foreach (string cellRow in cellRows)
                {
                    for (int i = 0; i < cellRow.Length; i++)
                    {
                        if (cellRow[i].Equals('O'))
                        {
                            row[colIndex] = Cell.Alive;
                            colIndex++;
                        }
                        else if (cellRow[i].Equals('X'))
                        {
                            row[colIndex] = Cell.Dead;
                            colIndex++;
                        }
                    }

                    cells[rowIndex] = row;
                    row = new Cell[width];
                    colIndex = 0;
                    rowIndex++;
                }
            }
            return cells;
        }

        // GET: UserGame/PlayActiveGame
        public ActionResult PlayActiveGame(string name)
        {
            //UserGame userGame = null;
            foreach (var game in listOfUserGames)
            {
                if (game.Name.Equals(name))
                {
                    Session["UserGame"] = game;
                    break;
                }
            }            
            
            var userGame = (UserGame)Session["UserGame"];

            Cell[][] cells = ConvertTo2dCellArray(userGame.Height, userGame.Width, userGame.Cells);

            TheGameOfLife theGameOfLife = new TheGameOfLife(userGame.Height, userGame.Width, cells);
           
            Session["TheGameOfLife"] = theGameOfLife;

            return View(userGame);
        }

        // POST: UserGame/PlayGameTick
        [HttpPost]
        public string PlayGameTick()
        {
            var activeGameOfLife = (TheGameOfLife)Session["TheGameOfLife"];

            activeGameOfLife.TakeTurn();

            return activeGameOfLife.ToString();
        }

        // GET: UserGame/SaveGame
        public ActionResult SaveGame(string name)
        {
            if (Session["User"] != null)
            {
                UserGame userGame = null;

                foreach (var item in listOfUserGames)
                {
                    if (item.Name.Equals(name))
                    {
                        userGame = item;
                        break;
                    }
                }

                userGame = (UserGame)Session["UserGame"];

                if (ModelState.IsValid)
                {
                    db.UserGames.Add(userGame);
                    db.SaveChanges();

                    return RedirectToAction("MySavedGames", "UserGame");
                }

                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        // GET: UserGame/MySavedGames
        public ActionResult MySavedGames()
        {
            var userGames = db.UserGames;

            return View(userGames.ToList());
        }

        // GET: UserGame/PlaySavedGame
        //public ActionResult PlaySavedGame()
        //{           
        //    return View();
        //}
    }
}
