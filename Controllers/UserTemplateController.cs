﻿using GameOfLife.Models.Database;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GameOfLife.Controllers
{
    public class UserTemplateController : Controller
    {
        private GameOfLifeEntities db = new GameOfLifeEntities();

        // GET: Templates
        public ActionResult Templates(string name)
        {
            var userTemplates = db.UserTemplates.Select(x => x);

            if (!string.IsNullOrWhiteSpace(name))
            {
                userTemplates = userTemplates.Where(x => x.Name.Contains(name));
            }

            return View(userTemplates.ToList());
        }

        // GET: MyTemplates
        public ActionResult MyTemplates(string name)
        {
            User user = (User)Session["User"];
            user = db.Users.Find(user.UserID);

            return View(user.UserTemplates.ToList());
        }

        // GET: UserTemplates/TemplateDetails/5
        public ActionResult TemplateDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }

            return View(userTemplate);
        }

        // GET: UserTemplates/CreateTemplate
        public ActionResult CreateTemplate()
        {
            return View();
        }

        // POST: UserTemplates/CreateTemplate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplate(UserTemplate userTemplate)
        {
            if (ModelState.IsValid)
            {
                User user = (User)Session["User"];
                userTemplate.UserID = user.UserID;

                db.UserTemplates.Add(userTemplate);
                db.SaveChanges();
                return RedirectToAction("MyTemplates", "UserTemplate");
            }

            return View(userTemplate);
        }

        // GET: UserTemplates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }
            return View(userTemplate);
        }

        // POST: UserTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            db.UserTemplates.Remove(userTemplate);
            db.SaveChanges();
            return RedirectToAction("MyTemplates", "UserTemplate");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
