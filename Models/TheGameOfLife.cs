﻿using System;
using System.Linq;

/*Code on lines 7 to 125 provided by Matthew Bolger.
  Accessed 12th August 2016.*/
namespace GameOfLife.Models
{
    public class TheGameOfLife
    {
        public TheGameOfLife(int height, int width)
        {
            if (height < 1 || width < 1)
            {
                throw new ArgumentException();
            }

            Height = height;
            Width = width;
        }
        
        public TheGameOfLife(int height, int width, Cell[][] cells) : this(height, width)
        {
            Cells = cells;
        }

        public int Height { get; }
        public int Width { get; }
        public Cell[][] Cells { get; set; }

        public void TakeTurn()
        {
            // Rules:
            //  1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.
            //  2. Any live cell with two or three live neighbours lives on to the next generation.
            //  3. Any live cell with more than three live neighbours dies, as if by over-population.
            //  4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            Cell[][] newCells = new Cell[Height][];
            for (int y = 0; y < Height; y++)
            {
                newCells[y] = new Cell[Width];
                for (int x = 0; x < Width; x++)
                {
                    Cell newCell;
                    int aliveNeighbours = AliveNeighbours(x, y);
                    switch (Cells[y][x])
                    {
                        case Cell.Alive:
                            if (aliveNeighbours < 2)
                            {
                                newCell = Cell.Dead;
                            }
                            else if (aliveNeighbours == 2 || aliveNeighbours == 3)
                            {
                                newCell = Cell.Alive;
                            }
                            else // More than 3.
                            {
                                newCell = Cell.Dead;
                            }
                            break;

                        case Cell.Dead:
                            newCell = aliveNeighbours == 3 ? Cell.Alive : Cell.Dead;
                            break;

                        default:
                            throw new InvalidOperationException();
                    }

                    newCells[y][x] = newCell;
                }
            }
            Cells = newCells;
        }

        private int AliveNeighbours(int xCoordinate, int yCoordinate)
        {
            int aliveNeighbours = 0;
            for (int y = yCoordinate - 1; y <= yCoordinate + 1; y++)
            {
                if (y < 0 || y >= Height)
                {
                    continue;
                }

                for (int x = xCoordinate - 1; x <= xCoordinate + 1; x++)
                {
                    if (x < 0 || x >= Width || (y == yCoordinate && x == xCoordinate))
                    {
                        continue;
                    }
                    if (Cells[y][x] == Cell.Alive)
                    {
                        aliveNeighbours++;
                    }
                }
            }
            return aliveNeighbours;
        }

        public override string ToString()
        {
            return CellUtilities.ToString(Cells);
        }

        public static Cell[][] GetCells(int height, int width, string[] lines)
        {
            if (height < 1 || width < 1 ||
               lines == null || lines.Length != height || lines.Any(x => x.Length != width))
            {
                throw new ArgumentException();
            }

            Cell[][] cells = new Cell[height][];
            for (int y = 0; y < height; y++)
            {
                cells[y] = new Cell[width];
                for (int x = 0; x < width; x++)
                {
                    cells[y][x] = CellUtilities.GetCell(lines[y][x]);
                }
            }

            return cells;
        }
    }
}
