﻿using FluentValidation;
using FluentValidation.Attributes;
using GameOfLife.Models.Database;
using System;
using System.ComponentModel.DataAnnotations;

/*Code on lines 17 to 95 provided by Matthew Bolger.
Accessed 12th August 2016.*/
namespace GameOfLife.Models
{
    public class UserGameMetadata
    {
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Height cannot be less than 1")]
        public int Height { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Width cannot be less than 1")]
        public int Width { get; set; }

        [Required]
        public string Cells { get; set; }
    }

    public class UserMetadata
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string LastName { get; set; }
    }

    public class UserTemplateMetadata
    {
        [Required]
        public string Name { get; set; }

        [Required, Range(1, 100)]
        public string Width { get; set; }

        [Required, Range(1, 100)]
        public string Height { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Cells { get; set; }
    }

    public class UserTemplateValidator : AbstractValidator<UserTemplate>
    {
        public UserTemplateValidator()
        {
            RuleFor(x => x).Must(x =>
            {
                try
                {
                    TheGameOfLife.GetCells(x.Height, x.Width, x.Cells.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None));
                    return true;
                }   
                catch
                {
                    return false;
                }
            }).WithMessage("Cells is invalid - must only be X's and O's.");

        }
    }

    namespace Database
    {
        [MetadataType(typeof(UserGameMetadata))]
        public partial class UserGame
        { }

        [MetadataType(typeof(UserMetadata))]
        public partial class User
        { }

        [MetadataType(typeof(UserTemplateMetadata))]
        [Validator(typeof(UserTemplateValidator))]
        public partial class UserTemplate
        { }
    }
}
